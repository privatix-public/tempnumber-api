# Temp-number API
This API allows you to get temporary numbers and receive SMS for online accounts verification and QA tests.
Most of our phone numbers are real mobile numbers (not virtual) coming from physical SIM cards.
This doubles your chances to receive SMS for account verification.
You can find out more about the service at
[https://temp-number.org](https://temp-number.org).

# Swagger (OpenAPI) docs

https://privatix-public.gitlab.io/tempnumber-api

# Introduction
In order to receive SMS, please:
1. Top up your balance through UI:
    - [https://temp-number.org](https://temp-number.org/app)
    - [Apple App Store](https://apps.apple.com/app/id1589908420)
    - [Google Play](https://play.google.com/store/apps/details?id=com.receive.sms_second.number)
2. Get price list by sending **GET** requests to `/pricelistByService`.

    In response you will get something like:
    ```json
    {
        "twitter": {
        "uk": 0.3,
        "us": 0.8,
        "us_v": 0.23
        },
        "facebook": {
        "de": 0.8,
        "jp": 2
        }
    }
    ```
3. Get temporary mobile phone number for specific service and country by sending 
    **POST** request to `/activations` while passing both **service** and **country** as request parameters.
    For example, in order to request activation code for Facebook in Germany set:
    ```json
    {
    "countryId": "de",
    "serviceId": "facebook"
    }
    ```

    In response you will get:
    - activation id
    - phone number

4. Send SMS to the phone number
5. Get SMS by sending request to `/acivations/<activation_id>`.
You should poll this endpoint while `status=smsRequested`.
Please, note that rate limits applies.

# Rate limits
In order to protect service from excessive use whether intended or unintended we are rate limiting request to API endpoints to maintain service availability.
Rates apply to each endpoint separately.
|  endpoint                                 |  method |  rate                       |
|  ---                                      |  ---    |  ---                        |
| /user/balance                             |  GET    |   10 request per minute     |
| /activations                              |  GET    |   10 request per minute     |
| /activations                              |  POST   |   80 requests per 2 minutes |
| /activations/:activation_id               |  GET    |   80 requests per 2 minutes |
| /services/:serviceId/countries/:countryId |  GET    |   80 requests per 2 minutes |
| /services/pricelistByService              |  GET    |   3 requests per minute     |
| /services/pricelistByCountry              |  GET    |   3 requests per minute     |

# Cross-Origin Resource Sharing
This API features Cross-Origin Resource Sharing (CORS) implemented in compliance with  [W3C spec](https://www.w3.org/TR/cors/).
And that allows cross-domain communication from the browser.
All responses have a wildcard same-origin which makes them completely public and accessible to everyone, including any code on any site.

# Authentication

In order to use this API you need to authenticate with API key. API key can be found under your profile. 
1. [Login](https://temp-number.org/app/create-account) into your account.
2. Go to Profile and press "Change API key".
3. Write down your API key. Please, note that **API key is displayed only once**. It can only be regenerated, but never restored.

You should send API key for all API requests in `x-api-key` header.